# Instalación
Ejecuta el siguiente comando para instalar la librería `sqlite3`:
```bash
npm install sqlite3
```

- Asegúrate de que el archivo `crebas.sql` se encuentre en la carpeta `db`.
- Ejecuta el script `crebas.js` para crear las tablas en la base de datos:

```bash
node db/crebas.js
```

- Asegúrate de que el archivo `inserts.sql` también esté en la carpeta db.
- Ejecuta el script `inserts.js` para insertar los datos iniciales:

```bash
node db/inserts.js
```

- Una vez que la base de datos esté configurada e inicializada, ejecuta con el siguiente comando:

```bash
npm run dev
```